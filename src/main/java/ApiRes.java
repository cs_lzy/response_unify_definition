import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Data;

/*
* @Description: 请求响应统一定义类
* @Author: LZY
* @Date: 2021/6/19 20:15
*/
@Data
@AllArgsConstructor
public class ApiRes {
    public static final Integer SUCCESS = 200;
    public static final Integer ERROR = 500;
    public static final Integer FAIL = 900;

    public static final Integer DB_ERROR = 5001;
    public static final Integer SYSTEM_ERROR = 5002;

    /** 业务响应码 **/
    private Integer code;

    /** 业务响应信息 **/
    private String msg;

    /** 数据对象 **/
    private Object data;

    /** 输出json格式字符串 **/
    public String toJSONString(){
        return JSON.toJSONString(this);
    }

    /** 业务处理成功 **/
    public static ApiRes ok(){
        return ok(null);
    }

    /** 业务处理成功 **/
    public static ApiRes ok(Object data){
        return new ApiRes(SUCCESS, "SUCCESS", data);
    }

    /** 通用业务处理失败 **/
    public static ApiRes fail(){
        return new ApiRes(FAIL, "业务处理失败", null);
    }

    /** 自定义错误信息, 根据异常返回详细的业务错误 **/
    public static ApiRes customFail(BusinessException ex){
        return new ApiRes(ex.getCode(), ex.getMessage(), null);
    }

    /** 服务器错误导致业务处理失败**/
    public static ApiRes error(){
        return new ApiRes(ERROR, "服务器错误", null);
    }

    /** 详细服务器错误**/
    public static ApiRes customError(Integer code, String msg){
        return new ApiRes(code, msg, null);
    }
}
