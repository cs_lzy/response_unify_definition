/*
* @Description: 业务异常定义类
* @Author: LZY
* @Date: 2021/6/19 19:57
*/
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = -7480022450501760611L;

    //  TODO 定义常用业务异常码
    public static final Integer PARAMETERERROR = 901; // 参数错误
    public static final Integer BUSINESSLOGICERROR = 902; // 业务逻辑错误，例如修改一个不存在的对象信息,用户不存在等
    public static final Integer UNAUTHORIZED = 903; // 未授权
    /**
     * 异常码
     */
    private Integer code;

    /**
     * 异常提示信息
     */
    private String message;

    public BusinessException() {
    }

    public BusinessException(Integer code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
